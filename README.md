# An-Array-Class-in-Java
public class  LessonArray 
{
	public static void main ( String[] args) 
	{
		 // Allocate memory for 5 integers 
		 int[]  anArray;
		 anArray = new int[5];
		 
		 // Initialize elements
		 anArray[0] = 10;
		 anArray[1] = 20;
		 anArray[2] = 30;
		 anArray[3] = 40;
		 anArray[4] = 50;
		 
		 System.out.println("Value at index 0:" + anArray[0]);
		 System.out.println("Value at index 1:" + anArray[1]);
		 System.out.println("Value at index 2:" + anArray[2]);
		 System.out.println("Value at index 3:" + anArray[3]);
		 System.out.println("Value at index 4:" + anArray[4]); 
	}
}

